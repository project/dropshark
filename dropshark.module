<?php

/**
 * @file
 * DropShark module code.
 */

/**
 * Registers dropshark_finalize_request() as a shutdown function.
 */
function dropshark_set_shutdown_function() {
  static $registered = FALSE;

  if (!$registered) {
    drupal_register_shutdown_function('dropshark_finalize_request');
    $registered = TRUE;
  }
}

/**
 * Finish processing on the end of the page request.
 *
 * Runs collectors which have been deferred until the end of the request, then
 * saves data from static request queue into persistent a queue.
 */
function dropshark_finalize_request() {
  /** @var \Drupal\dropshark\Queue\QueueInterface $queue */
  $queue = Drupal::service('dropshark.queue');

  // Perform deferred processing.
  $queue->processDeferred();

  // Transmit if immediate transmit is indicated.
  if ($queue->needsImmediateTransmit()) {
    $queue->transmit();
  }

  // Persistently store remaining items.
  $queue->persist();
}

/**
 * Implements hook_cron().
 */
function dropshark_cron() {
  $config = \Drupal::configFactory()->get('dropshark.settings');
  if ($config->get('cron')) {
    /** @var \Drupal\dropshark\Collector\CollectorManagerInterface $collectorManager */
    $collectorManager = Drupal::service('plugin.manager.dropshark_collector');
    $collectorManager->collect(['all'], [], TRUE);
  }
}
