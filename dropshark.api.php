<?php

/**
 * @file
 * Hooks provided by the DropShark module.
 */

/**
 * Alter DropShark Collector plugin definitions.
 *
 * @param array $collectors
 *   An array of DropShark Collector plugin definitions.
 */
function hook_dropshark_collector_info_alter(array &$collectors) {

}
